<div style="margin:20px">

  <h2 style="text-align:center">
    "Company Logo"
  </h2>

  <h3 style="text-align:center">
    IMPORTANT GROUND TRANSPORTATION INFORMATION
  </h3>

  <h5 style="margin-top:30px">Dear : <%name%> </h5>
  <p style="font-weight:bold">
      Our Records show that you will departing from <%city%> on:<br/>
      Date: <%time%> <br/>
      Hotel Depart Time: <%hotel_time%><br/>
      Airline/Flight: <%flight%><br/>
      Hotel Depart Time: <%flight_time%><br/>
  </p>
  <p>
    your transportation to the <%city%> international Airport is scheduled to depart from the resort's <br/>
    front drive <br/><br/>

    please call the bell desk at least 30 mintues prior to departure if you would like luggage <br>
    assistence. <br><br>

    please proceed to the resort's front drive al least (10) mintues prior to your vehicle departure <br>
    time. A Destination and conference Service representative will assist you from there. Please <br>
    identify and verify your luggage is on vehicle prior to boarding.<br>

    <h4>
      If you have any changes with your flights and transportation,please contact destination & <br>
      Confenrence Service at <span style="text-decoration:underline">520-400-4477</span><br>
      <br>
      Thank You
    </h4>
  </p>
</div>
