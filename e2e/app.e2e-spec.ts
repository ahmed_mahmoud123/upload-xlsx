import { SocialScriptPage } from './app.po';

describe('social-script App', function() {
  let page: SocialScriptPage;

  beforeEach(() => {
    page = new SocialScriptPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
