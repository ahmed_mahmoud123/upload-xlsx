import { Component, OnInit } from '@angular/core';
import {NewsService} from '../../providers/news.service';

declare var XLSX:any;

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css'],
  providers:[NewsService]
})

export class AddNewComponent implements OnInit {
  data:Array<any>=[];
  checkAll:boolean=false;
  message:string="";
  constructor(private newsService:NewsService) { }

  ngOnInit() {
  }

  imageUploaded(event){
    this.parseXLSX(event.target.files[0])
  }

  parseXLSX(file) {
    let self=this;
    var reader = new FileReader();
    reader.onload = function(){
        var fileData = reader.result;
        var wb = XLSX.read(fileData, {type : 'binary'});
        wb.SheetNames.forEach(function(sheetName){
        var rowObj =XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
        var jsonObj = JSON.stringify(rowObj);
          self.data=JSON.parse(jsonObj);
          self.data.filter(user=>{
            return user['Depart Date']!=null && user['Depart Date']!='';
          });
          let _id=0;
          self.data.map(user=>{
            user._id=++_id;
            user.checked=false;
            return user;
          });
        })
    };
    reader.readAsBinaryString(file);
  };

  send(){
    this.message="waiting , sending ....";
    let selected_user=this.data.filter(user=>{
      return user.checked;
    });

    if(selected_user.length == 0){
      this.message = "You must upload a Manifest file and select at least one email before you hit the Send Email button"
    }else{
      this.newsService.send(selected_user).then(result=>{
        this.message="Done";
        setTimeout(()=>{
          this.message="";
        },3000);
      });
    }
  }


  onChange(value,checked){
  this.checkAll=true;
  for (let i = 0; i < this.data.length; i++) {
      if(value==this.data[i]['_id']){
        this.data[i].checked=checked;
      }
      if(!this.data[i].checked)
        this.checkAll=false;
  }
}

toggleAll(checked){
  this.checkAll=checked;
  for (let i = 0; i < this.data.length; i++) {
        this.data[i].checked=checked;
  }
}

}
