import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule ,Routes } from '@angular/router'
import { AppComponent } from './app.component';

import { AddNewComponent } from './components/add-new/add-new.component';
import { Ng2CompleterModule } from "ng2-completer";

const routes:Routes=[

  {
    path:"",
    component:AddNewComponent
  }
]
@NgModule({
  declarations: [
    AppComponent,
    AddNewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    Ng2CompleterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
