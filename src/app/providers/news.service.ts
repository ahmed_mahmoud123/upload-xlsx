import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class NewsService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://97.74.4.85/upload-xlsx";
    this.headers = new Headers ({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
  }



  send(users){
    return this.http.post(`${this.api_url}/index.php`,"users="+JSON.stringify(users),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

}
